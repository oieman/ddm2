POSTS_PER_PAGE = 6;

//INIT FIRST PAGINATION SLIDE
jQuery('.pagination').slick({dots: true});

jQuery('#month_filter').bind('change', function(evt) {
    var optionSelected = jQuery("option:selected", this);
    var valueSelected = this.value;

    console.log(valueSelected);

    var data = valueSelected;
    sendRequest( data, function( response ) {
            //jQuery("#events_list").empty();
            jQuery("#page-content").empty();
            
            datas = JSON.parse(response);
            page_number = Math.ceil(datas.length/POSTS_PER_PAGE);

            pages_html = "<div class='pagination'>";
            j = 0;
            page_content = "<div class='pagination_slide'>";
            for (var i = 0; i < datas.length; i++) {
        		console.dir(datas[i]);
        		page_content += datas[i]["post_title"]+"<br/>";
        		j++;
        		if( j == POSTS_PER_PAGE){
            		console.log(page_number);
           			j = 0;
        			page_content += "</div>";
        			pages_html += page_content;
            		page_content = "<div class='pagination_slide'>";
        		}
        	}
        	if(page_content != ""){
        		page_content += "</div>";
        		//pages.push(page_content);
        		pages_html += page_content;
        		page_content = "";
        	}
        	pages_html += "</div>";

        	jQuery('#page-content').html(pages_html);
        	jQuery('.pagination').slick({dots: true});

    } );

});

var sendRequest = function( data, callback ) {
    jQuery.post(
	    ajaxurl,
	    {
	        'action': 'ddm_ajax_get_month_events',
	        'param': data
	    },
	    function(response){
	    	 if( callback != undefined )
	                callback( response );
	    }
	);
}

