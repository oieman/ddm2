<?php


$params = array_merge( array(
   array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Text"),
         "param_name" => "text",
         "value" => __("Default params value"),
         "description" => __("Description for foo param.")
   ),
   array(
      'type' => 'dropdown',
      'heading' => __( 'Color', 'js_composer' ),
      'param_name' => 'color',
      'description' => __( 'Select button color.', 'js_composer' ),
      // compatible with btn2, need to be converted from btn1
      'param_holder_class' => 'vc_colored-dropdown vc_btn3-colored-dropdown',
      'value' => array(
            // Btn1 Colors
            __( 'Classic White', 'js_composer' ) => 'white',
            __( 'Classic Blue', 'js_composer' ) => 'blue',
            // + Btn2 Colors (default color set)
         ),
      'std' => 'grey',
      // must have default color grey
      'dependency' => array(
         'element' => 'style',
         'value_not_equal_to' => array(
            'custom',
            'outline-custom',
         ),
      ),
   )
   ));

return array(
   'name' => __( 'DDM Agenda', 'js_composer' ),
   'base' => 'vc_ddm_calendar',
   'icon' => 'icon-wpb-ui-button',
   'category' => array(
      __( 'Content', 'js_composer' ),
   ),
   'description' => __( 'DDM Agenda', 'js_composer' ),
   'params' => $params,
   //'js_view' => 'VcButton3View',
   //'custom_markup' => '<div class="vc_ddm-container"> DDM Agenda</div>',
);