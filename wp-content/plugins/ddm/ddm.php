<?php
/*
Plugin Name: Divan du Monde & Madame Arthur
Description: Fonctionnalités et styles du Divan du monde.
Version: 1.0
Author: Yacine Arhaliass
Author URI: http://www.arhaliass.net
Dependencies: Salient Visual Composer
*/

global $DDM_DIR;
	$DDM_DIR =  plugin_dir_url( __FILE__ );

define( 'WP_DEBUG', true );

class DDM_Plugin
{
    public function __construct()
    {

    	/*if ( ! $vc_manager ) {
    		$vc_manager = $vc_manager.getInstance();
			vc_lean_map( 'vc_ddm_calendar', null, $DDM_DIR . '/shortcode-vc-ddm-calendar.php' );
		}*/

		/**
		* TODO: VC calendar is currently remaining on salient_js_composer and must be moved in here.
		* file in  salient_js_composer $vc_config_path . '/ddm/shortcode-vc-ddm-calendar.php'
		* include in lean-map
		* add_shortcode( 'vc_ddm_calendar', 'vc_ddm_calendar_func'); in js_composer
		*/
	}

}

global $ddm_plugin;
$ddm_plugin = new DDM_Plugin();


function ddm_ajax_get_month_events(){
	echo json_encode(ddm_get_month_events($_POST['param']));
	die();
}

function ddm_get_month_events($selected_month) {
	$selected_date = explode(":", $selected_month);

	$year = $selected_date[0];
	$month = $selected_date[1];

	$next_year = $year;
	$next_month = (int)$month + 1;

	if($next_month == 13){
		$next_month = 01;
		$next_year += 1; 
	}

	$start_date = ""+$year;
	$end_date = ""+$next_year;

	$start_date .="-";
	$end_date .= "-";

	$start_date .= ""+$month;
	$end_date .= ""+$next_month;

	$start_date .="-01 00:00";
	$end_date .= "-01 00:00";

	$post_data = tribe_get_events( array(
		'start_date' => $start_date,
		'end_date'   => $end_date
	));
	foreach ($post_data as $key => $value) {
		$customfield1 = get_post_meta( $value->ID, 'digitick-ticket', true );
		if($customfield1){
			$value->digitick = $customfield1;
		}

		$customfield2 = get_post_meta( $value->ID, 'weezevent-ticket', true );
		if($customfield2){
			$value->weezevent = $customfield2;
		}

		$customfield3 = get_post_meta( $value->ID, 'fnac-ticket', true );
		if($customfield3){
			$value->fnac = $customfield3;
		}
		//prix
		$customfield4 = get_post_meta( $value->ID, '_EventCost', true );
		if($customfield4){
			$value->price = $customfield4;
		}

		//devise
		//$costs = tribe_get_event_meta( $event->ID, '_EventCost', false );
		$customfield5 = get_post_meta( $value->ID, '_EventCurrencySymbol', true );
		if($customfield5){
			$value->currency = $customfield5;
		}

		//lien
		$customfield6 = get_post_meta( $value->ID, '_EventURL', true );
		if($customfield6){
			$value->link = $customfield6;
		}
		$value->img = get_the_post_thumbnail_url($value->ID);
		// 2016-11-01 17:00:00
		//$date = date_create($value->EventStartDate,"Y-m-d H:i:s");
		$date = strtotime($value->EventStartDate);
		$value->hour =date("H",$date);
		$date_d = date("d",$date);
		$date_m = date("m",$date);
		$date_y = date("Y",$date);
		$MONTHS = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre");
		$value->date = $date_d . "&nbsp" ;
		$value->date .= $MONTHS[$date_m-1] . "&nbsp";
		$value->date .= $date_y;
		$post_data[$key] = $value;
	}

	return $post_data;
}	
function vc_ddm_slider_func( $atts, $content){
	$outputs="";
	$outputs .= "<div class='header_slider_container'>";
		$outputs .= "<div class='header_counter'>";
		$outputs .= "<div class='header_counter_wrapper'>";
				$outputs .= "<div id='header_counter_disk'>";
				$outputs .= "</div>";
				$outputs .= "<div id='header_counter_text'>CHANSON";
				$outputs .= "</div>";
				$outputs .= "<div id='header_counter_slide_wrapper'>";
					$outputs .= "<div id='header_counter_slide'>1";
					$outputs .= "</div>";
					$outputs .= "<div id='header_counter_total'>/5";
					$outputs .= "</div>";
				$outputs .= "</div>";
			$outputs .= "</div>";
		$outputs .= "</div>";
		$outputs .= "<div class='header_slider'>";
	$posts = query_posts( 'cat=5' );

	foreach ($posts as $post) {
    	$outputs .= "<div class='header_slider_slide'>";
		$outputs .= "<br/><br/>";
		$outputs .= $post->post_content;
		$outputs .= "</div>";
	}
	$outputs .= "</div>";
	$outputs .= "</div>";

	return $outputs;
}

function vc_ddm_calendar_func( $atts, $content) {

	$POSTS_PER_PAGE = 6;
	$MAX_MONTHS = 6;

    extract( shortcode_atts( array(
        'text' => 'text',
        'color' => 'color'
    ), $atts ) );

	$current_date = date("Y:m");

    $end_content = ' <div class="vc_ddm_calendar">';
    //$end_content .= '<h2 class="text-center">'.$text."//".$color.'</h2>';
    
    $end_content .= "<div class='events_list_header'>";
    $end_content .= "<div class='bleufonce-color'><h1>AGENDA</h1></div>";
    $end_content .= "<div class='hr'><hr align='center'/></div>";
    $end_content .= "<select id='month_filter'>";
	$end_content .=  ddm_get_next_months($MAX_MONTHS);
	$end_content .=	"</select>";
	$end_content .=	"</div>";

	$end_content .= "<div id='events_list_container'>";
	$end_content .= "<div id='page-content'>";
    $end_content .= "<div class='pagination'>";

    $events_datas = array();
    $events_datas = json_decode(json_encode(ddm_get_month_events($current_date)));

    //$end_content .= var_dump($datas);

    //$page_number = ceil(count($datas)/$POSTS_PER_PAGE);

    $j = 0;
    $i = 0;
    $page_content = "<div class='pagination_slide'>";
    foreach ($events_datas as $post) {

    	$page_content .= "<div class='pagination_slide_card front' id='card_".$i."'>";
            $page_content .= "<div class='pagination_slide_card_front'>";
                $page_content .= "<div class='pagination_slide_col pagination_slide_front_col1'>";
                    $page_content .= "<img src='".$events_datas[$i]->img."' />";
                $page_content .= "</div>";
                $page_content .= "<div class='pagination_slide_col pagination_slide_front_col2'>";
                    $page_content .= "<div class='pagination_slide_front_date'>";
                    $page_content .= $events_datas[$i]->date."</div>";
                    $page_content .= "<div class='pagination_slide_front_title'>";
                    $page_content .= $events_datas[$i]->post_title."</div>";
                    $page_content .= "<div class='pagination_slide_front_text'>";
                    $page_content .= $events_datas[$i]->post_content."</div>";
                $page_content .= "</div>";

            $page_content .= "</div>";

            $page_content .= "<div class='pagination_slide_card_back'>";
	            $page_content .= "<div class='pagination_slide_card_back_wrapper'>";
	                $page_content .= "<div class='pagination_slide_col pagination_slide_back_col1'>";
                    	$page_content .= "<div class='pagination_slide_back_link'>";
	                    $page_content .= "<a href='".$events_datas[$i]->link."' target='_blank'>EN SAVOIR PLUS</a></div>";

                        	$page_content .= "<div class='pagination_slide_back_hour'>";
                                $page_content .= "<div class='pagination_slide_back_hour_left'>HORAIRE</div>";
                                $page_content .= "<div class='pagination_slide_back_hour_right'> ".$events_datas[$i]->hour."&nbspH</div>";
                            $page_content .= "</div>";
                            $page_content .= "<div class='pagination_slide_back_price'>";
                                $page_content .= "<div class='pagination_slide_back_price_left'>TARIF</div>";
                                $page_content .= "<div class='pagination_slide_back_price_right'> ".$events_datas[$i]->price."&nbsp€</div>";
                            $page_content .= "</div>";
	                $page_content .= "</div>";
					if((empty($events_datas[$i]->fnac)) && (empty($events_datas[$i]->digitick)) && (empty($events_datas[$i]->weezevent))){}
	            	else{
	                $page_content .= "<div class='pagination_slide_col pagination_slide_back_col2'>";
	            
                        $page_content .= "<div class='pagination_slide_back_ticket'> -&nbsp BILLETTERIE &nbsp-</div>";
                        if(!empty($events_datas[$i]->fnac)){
                    		$page_content .= "<div class='pagination_slide_back_fnac pagination_slide_back_social'>";
	                    	$page_content .= "<a href='".$events_datas[$i]->fnac."' target='_blank'>FNAC</a></div>";
                        }
                    	
                    	if(!empty($events_datas[$i]->digitick)){
                    		$page_content .= "<div class='pagination_slide_back_digitick pagination_slide_back_social'>";
	                    	$page_content .= "<a href='".$events_datas[$i]->digitick."' target='_blank'>DIGITICK</a></div>";
	                    }
                    	if(!empty($events_datas[$i]->weezevent)){
                    		$page_content .= "<div class='pagination_slide_back_weezevent pagination_slide_back_social'>";
	                    	$page_content .= "<a href='".$events_datas[$i]->weezevent."' target='_blank'>WEEZEVENT</a></div>";
	                    }
	                $page_content .= "</div>";
	                }
	            $page_content .= "</div>";
                	


            $page_content .= "</div>";
        $page_content .= "</div>";
    	$i++;
		$j++;
		if( $j == $POSTS_PER_PAGE){
   			$j = 0;
			$page_content .= "</div>";
			$end_content .= $page_content;
    		$page_content = "<div class='pagination_slide'>";
		}/**/
	}
	if($page_content != ""){
		$page_content .= "</div>";
		$end_content .= $page_content;
		$page_content = "";
	}

	$end_content .= "</div>";
	$end_content .= "</div>";
	$end_content .= "</div>";
    $end_content .= '</div>'; 
    return $end_content; 

}

function ddm_get_next_months($n){
	// n next months
$MONTHS = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre");

	$selected_months = array();
	$outputs ="";

	$current_month = date("m");
	$current_year = date("Y");

	$value = array();
	for($i=0; $i<=$n; $i++){
		$month = (int)$current_month + $i;
		$year = $current_year;
		if($month > 12){
			$month -= 12;
			$year = (int)$current_year + 1;
		}
		$key = "";
		$key .= $year;
		$key .=":";
		$key .= $month;
		if($i==0){
		$outputs .= "<option selected value=";
		}else{
			$outputs .= "<option value=";
		}

		$outputs .= $key;
		$outputs .= ">";
		$outputs .= $MONTHS[$month-1];
		$outputs .= " ";
		$outputs .= $year;
		$outputs .= "</option>";
	}
	return $outputs;
}


function vc_ddm_separator_func($atts, $content) {
	$outputs = "<div class='section_separator'>";
	$outputs .= "<div class='line_separator'></div>";
	$outputs .= "<div class='wave_separator_container'>
	<div class='wave_separator'>
	  <svg xmlns='http://www.w3.org/2000/svg'
	     width='80px' height='60px'
	     viewBox='5 0 80 60'>
	    <path class='wave' 
	        fill='none' 
	        stroke='#080a6e' 
	        stroke-width='4'
	        stroke-linecap='round'>
	    </path>
	  </svg>
	</div></div>";
	$outputs .= "<div class='line_separator'></div>";
	$outputs .= "</div>";
	return $outputs;
}

function ddm_add_scripts() {
	wp_enqueue_script( 'pagination', plugin_dir_url( __FILE__ ).'js/slick.js', array('jquery'), '', true);
	wp_enqueue_script( 'script', plugin_dir_url( __FILE__ ).'js/ddm.js', array('jquery'), '', true);
	wp_enqueue_style( 'pagination', plugin_dir_url( __FILE__ ).'css/slick.css');
	wp_enqueue_style( 'paginationtheme', plugin_dir_url( __FILE__ ).'css/slick-theme.css');
	wp_enqueue_style( 'ddmstyle', plugin_dir_url( __FILE__ ).'css/ddmstyle.css');
	// pass Ajax Url to script.js
	wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );

	wp_enqueue_style( 'fontstyle', "https://cloud.typography.com/7777156/7931172/css/fonts.css");
	wp_enqueue_style( 'fontstyle2', "https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700");


}
add_action( 'wp_ajax_ddm_ajax_get_month_events', 'ddm_ajax_get_month_events' );
add_action( 'wp_ajax_nopriv_ddm_ajax_get_month_events', 'ddm_ajax_get_month_events' );
add_action('wp_enqueue_scripts', 'ddm_add_scripts',12);
add_shortcode( 'vc_ddm_calendar', 'vc_ddm_calendar_func');
add_shortcode( 'vc_ddm_slider', 'vc_ddm_slider_func');
add_shortcode( 'vc_ddm_separator', 'vc_ddm_separator_func');