POSTS_PER_PAGE = 6;
//INIT FIRST PAGINATION SLIDE
jQuery('.pagination').slick({
    dots: true, arrows: false,centerMode: false, swipe:false,
    customPaging : function(slider, i) {
        var thumb = jQuery(slider.$slides[i]).data();
        return '<a>'+(i+1)+'</a>';
    },
});

var prev = "<button type='button' data-role='none' id='slick-prev' class='slick-prev slick-arrow' aria-label='Prev' role='button' style='display: block;'>Prev</button>";
var next = "<button type='button' data-role='none' id='slick-next'  class='slick-next slick-arrow' aria-label='Next' role='button' style='display: block;'>Next</button>";

jQuery("#agenda .slick-dots").wrap("<div class='custom_nav_wrapper'></div'>");
jQuery("#agenda .slick-dots").before(prev);
jQuery("#agenda .slick-dots").after(next);

jQuery("#slick-prev").on("click", function(){
    jQuery('.pagination').slick('slickPrev');
})
jQuery("#slick-next").on("click", function(){
    jQuery('.pagination').slick('slickNext');
})


jQuery('.header_slider').slick({
    arrows: false,
    autoplay: true,
    dots: true, 
    vertical:true,
    verticalSwiping: true, 
    speed: 1000,
    autoplaySpeed: 2000,
    useTransform: true,
    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
    adaptiveHeight: true,
    /*customPaging: function (slider, i) {
        //FYI just have a look at the object to find available information
        //press f12 to access the console in most browsers
        //you could also debug or look in the source
        console.dir(slider.slideCount);
        //return  (i + 1) + '/' + slider.slideCount;
    }*/
});

SLIDE_NUMBER = jQuery(".header_slider").slick("getSlick").slideCount;

jQuery("#header_counter_total").html(" / "+SLIDE_NUMBER);

jQuery('.header_slider').on('afterChange', function(event, slick, currentSlide){
    jQuery("#header_counter_slide").html(currentSlide+1);
});

jQuery('#month_filter').change( function(evt) {

    var optionSelected = jQuery("option:selected", this);
    var valueSelected = this.value;

    //console.log(valueSelected);
    jQuery("#page-content").empty();

    jQuery("#page-content").html("<div class='loading'></div>")

    var data = valueSelected;
    sendRequest( data, function( response ) {
            //jQuery("#events_list").empty();
            
            datas = JSON.parse(response);
            page_number = Math.ceil(datas.length/POSTS_PER_PAGE);

            pages_html = "<div class='pagination'>";
            j = 0;
            page_content = "<div class='pagination_slide'>";
            for (var i = 0; i < datas.length; i++) {
        		//console.dir(datas[i]);
                page_content += "<div class='pagination_slide_card front' id='card_"+i+"'>";
                    page_content += "<div class='pagination_slide_card_front'>";
                        page_content += "<div class='pagination_slide_col pagination_slide_front_col1'>";
                            page_content += "<img src='"+datas[i]["img"]+"' />";
                        page_content += "</div>";
                        page_content += "<div class='pagination_slide_col pagination_slide_front_col2'>";


                            page_content += "<div class='pagination_slide_front_date'>";
                            page_content += datas[i]["date"]+"</div>";
                            page_content += "<div class='pagination_slide_front_title'>";
                            page_content += datas[i]["post_title"]+"</div>";
                            page_content += "<div class='pagination_slide_front_text'>";
                            page_content += datas[i]["post_content"]+"</div>";
                        page_content += "</div>";

                    page_content += "</div>";

                    page_content += "<div class='pagination_slide_card_back'>";
                        page_content += "<div class='pagination_slide_card_back_wrapper'>";
                            page_content += "<div class='pagination_slide_col pagination_slide_back_col1'>";
                                page_content += "<div class='pagination_slide_back_link'>";
                                page_content += "<a href='"+datas[i]["link"]+"' target='_blank'>EN SAVOIR PLUS</a></div>";
                                page_content += "<div class='pagination_slide_back_hour'>";
                                page_content += "<div class='pagination_slide_back_hour_left'>HORAIRE</div>";
                                page_content += "<div class='pagination_slide_back_hour_right'> "+datas[i]["hour"]+"&nbspH</div>";
                                page_content += "</div>";
                                page_content += "<div class='pagination_slide_back_price'>";
                                page_content += "<div class='pagination_slide_back_price_left'>TARIF</div>";
                                page_content += "<div class='pagination_slide_back_price_right'> "+datas[i]["price"]+"&nbsp€</div>";
                                page_content += "</div>";
                            page_content += "</div>";

                            page_content += "<div class='pagination_slide_col pagination_slide_back_col2'>";
                                page_content += "<div class='pagination_slide_back_ticket'> -&nbspBILLETERIE&nbsp-</div>";
                                page_content += "<div class='pagination_slide_back_fnac pagination_slide_back_social'>";
                                page_content += "<a href='"+datas[i]["fnac"]+"' target='_blank'>FNAC</a></div>";
                                page_content += "<div class='pagination_slide_back_digitick pagination_slide_back_social'>";
                                page_content += "<a href='"+datas[i]["digitick"]+"' target='_blank'>DIGITICK</a></div>";
                                page_content += "<div class='pagination_slide_back_weezevent  pagination_slide_back_social'>";
                                page_content += "<a href='"+datas[i]["weezevent"]+"' target='_blank'>WEEZEVENT</a></div>";
                            page_content += "</div>";

                        page_content += "</div>";
                    page_content += "</div>";
                page_content += "</div>";

        		j++;
        		if( j == POSTS_PER_PAGE){
            		console.log(page_number);
           			j = 0;
        			page_content += "</div>";
        			pages_html += page_content;
            		page_content = "<div class='pagination_slide'>";
        		}
        	}
        	if(page_content != ""){
        		page_content += "</div>";
        		//pages.push(page_content);
        		pages_html += page_content;
        		page_content = "";
        	}
        	pages_html += "</div>";

        	jQuery('#page-content').html(pages_html);
        	jQuery('.pagination').slick({
                dots: true, arrows: false,centerMode: false, swipe:false,
                customPaging : function(slider, i) {
                    var thumb = jQuery(slider.$slides[i]).data();
                    return '<a>'+(i+1)+'</a>';
                },
            });

            var prev = "<button type='button' data-role='none' id='slick-prev' class='slick-prev slick-arrow' aria-label='Prev' role='button' style='display: block;'>Prev</button>";
            var next = "<button type='button' data-role='none' id='slick-next'  class='slick-next slick-arrow' aria-label='Next' role='button' style='display: block;'>Next</button>";

            jQuery("#agenda .slick-dots").wrap("<div class='custom_nav_wrapper'></div'>");
            jQuery("#agenda .slick-dots").before(prev);
            jQuery("#agenda .slick-dots").after(next);

            jQuery("#slick-prev").on("click", function(){
                jQuery('.pagination').slick('slickPrev');
            })
            jQuery("#slick-next").on("click", function(){
                jQuery('.pagination').slick('slickNext');
            })

            jQuery(".pagination_slide_card").on("click", function(e){
                flipCard(e)
            });
            jQuery(".pagination_slide_card a").bind ("mousedown mouseup click", function (e) {
                e.stopPropagation();
            } );
    } );

});

jQuery(".pagination_slide_card").on("click", function(e){
    flipCard(e)
});
jQuery(".pagination_slide_card a").bind ("mousedown mouseup click", function (e) {
    e.stopPropagation();
} );

/**/function leaveCard(e){
}
function enterCard(e){
}

function flipCard(e){
    if(jQuery(e.currentTarget).hasClass("front")){
        jQuery(e.currentTarget).removeClass("front");
        jQuery(e.currentTarget).addClass("back");
    }else if(jQuery(e.currentTarget).hasClass("back")){
        jQuery(e.currentTarget).removeClass("back");
        jQuery(e.currentTarget).addClass("front");
    }
}

var sendRequest = function( data, callback ) {
    jQuery.post(
	    ajaxurl,
	    {
	        'action': 'ddm_ajax_get_month_events',
	        'param': data
	    },
	    function(response){
	    	 if( callback != undefined )
	                callback( response );
	    }
	);
}


/**
* WAVE SEPARATOR
*/
const m = 0.512286623256592433;

function buildWave(w, h) {

  const a = h / 4;
  const y = h / 2;

  const pathData = [
    'M', w * 0, y + a / 2,
    'c',
    a * m, 0, -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,

    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a,
    's', -(1 - a) * m, a,
    a, a,
    's', -(1 - a) * m, -a,
    a, -a
  ].join(' ');

  jQuery( ".wave" ).each(function( index,obj ) {
        jQuery( this ).attr('d', pathData);
    });
}

buildWave(90, 60);

/**
* MOUSE ANIMATION
*/

 var init=function(){
    jQuery("#mouse").removeClass();
  },
  unhide=function(){
    jQuery("#mouse").addClass("unhide");
  },
  still=function(){
    jQuery("#mouse").removeClass();
    jQuery("#mouse").addClass("still");
  },
  scroll=function(){
    jQuery("#mouse").removeClass();
    jQuery("#mouse").addClass("scroll");
  },
  x=0;
  setInterval(function(){
    if(x==3){
      init();
      x++;
    }else if(x==4){
      unhide();
      x++;
    }else if(x==6){
      still();
      x++;
    }else if(x==10){
      scroll();
      x=0;
    }else{
      x++;
    }
  },250);

/*jQuery("#carte-hover-left").hide();
jQuery("#carte-hover-right").hide();

  jQuery("#carte-image-wrapper-left").mouseenter(function(){
    console.log("mouseenter");
      jQuery("#carte-hover-left").show();
  });

  jQuery("#carte-image-wrapper-left").mouseout(function(){
    console.log("mouseout");
      jQuery("#carte-hover-left").hide();
  });*/

jQuery('#carte-image-left img').hover(function () {
      jQuery(this).css("cursor", "pointer");
      this.src = '/wp-content/uploads/2016/12/cocktails2.png';
    }, function () {
      this.src = '/wp-content/uploads/2016/10/carte1.png';
    });
jQuery('#carte-image-right img').hover(function () {
      jQuery(this).css("cursor", "pointer");
      this.src = '/wp-content/uploads/2016/12/plats2.png';
    }, function () {
      this.src = '/wp-content/uploads/2016/10/carte2.png';
    });
/*
  jQuery("#carte-image-wrapper-right").mouseenter(function(){
    console.log("2");
      jQuery("#carte-hover-right").show();
  });
  jQuery("#carte-image-wrapper-right").mouseout(function(){
    console.log("4");
      jQuery("#carte-hover-right").hide();
  });*/